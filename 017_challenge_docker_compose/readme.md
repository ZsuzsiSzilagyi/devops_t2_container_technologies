## Your challenge if you choose to accept it
1. Clone this git repository [Awesome Compose](https://github.com/docker/awesome-compose)
1. Deploy locally a Python app. For tips and instructions you can follow the description in the flask folder (https://github.com/docker/awesome-compose/tree/master/flask)
1. Deploy Prometheus server with Graphana. For tips and instructions you can follow the description in the flask folder (https://github.com/docker/awesome-compose/tree/master/prometheus-grafana)
1. See if you can access port 9090 and 3000. On the latter you should be able to log in with admin/grafana
1. Clean up the workspace.