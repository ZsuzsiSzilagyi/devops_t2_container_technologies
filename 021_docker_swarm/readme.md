## Key features of Docker Swarm
* Decentralized design
* Declarative service model
* Scaling
* Desired state reconciliation
* Multi-host networking:
* Service discovery
* Load balancing
* Secure by default
* Rolling updates

## Architecture of Swarm

First, the provider independent terms
* Node: physical host, which can take place in the cluster
* Cluster: set of co-working nodes. Basically there are two types of

nodes: managers and workers. (This may not distinct sets.)
* Cluster management: set of nodes that maintains the cluster, assign tasks, etc.
* Cluster workers: running the assigned tasks
* Service: an abstraction of a container, including one or more parallel instance of the same container


These are the essence of the concepts. There are differences in the meanings at particular vendors!

## Key terms in Docker Swarm
* A swarm is a set of nodes with at least one master node and several worker nodes that can be virtual or physical machines.
* A service is the tasks a manager or agent nodes must perform on the swarm, as defined by a swarm administrator. A service defines which container images the swarm should use, and which commands the swarm will run in each container.
* Manager node. When you deploy an application into a swarm, the manager node provides several 
functions: it delivers work (in the form of tasks) to worker nodes, and it also manages the state of the swarm to which it belongs. The manager node can run the same services worker nodes do, but you can also configure them to only run manager node-related services.
* Worker nodes run tasks distributed by the manager node in the swarm. Each worker node runs an agent that reports back to the master node about the state of the tasks assigned to it, so the manager node can keep track of services and tasks running in the swarm.
* Tasks are Docker containers that execute the commands you defined in the service. Manager nodes assign tasks to worker nodes, and after this assignment, the task cannot be moved to another worker. If the task fails in a replica set, the manager will assign a new version of that task to another available node in the swarm.

## Manager nodes
* They handle cluster management tasks:
* maintaining cluster state
* scheduling services
* serving swarm mode HTTP API endpoints
* They using the Raft protocol to keep themselves consistent
* An N manager cluster tolerates the loss of at most (N-1)/2 managers.
* Adding more managers does NOT mean increased scalability or higher performance! In general, the opposite is true.

![](img/manager_nodes.png)

## Worker nodes
* By default, all managers are also workers.
* Worker nodes are instances of Docker Engine whose sole purpose is to execute containers.

## Services, tasks, and containers
* When you deploy the service to the swarm, the swarm manager accepts your service definition as the desired state for the service.
* Manager schedules the service on nodes in the swarm as one or more replica tasks.
* A task is the atomic unit of scheduling within a swarm. Each task invokes exactly one container
![](img/service_task_containers.png)