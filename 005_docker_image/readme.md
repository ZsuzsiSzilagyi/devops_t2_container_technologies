## What's In An Image (And What Isn't)
* Images are made up of app binaries, dependencies, and metadata
* Don't contain a full OS
* Usually use a Dockerfile recipe to create them
* Stored in your Docker Engine image cache
* Living in a repository (Docker Hub by default)

## Images are made of layers
* Images are made up of file system changes
and metadata
* Each layer is uniquely identified and only
stored once on a host
* This saves storage space on host and
transfer time on push/pull
* A container is just a single read/write layer
on top of image

![](img/container.png)

## Creating images
* Created Using a Dockerfile or committing a containers changes back to an image
* Commands in a Dockerfile create layers
* Once an image created, they stored in Docker Engine image cache
* Move images in/out of cache via:
* local filesystem via tarballs
* push/pull to a remote "image registry" (e.g. Docker Hub)

## What is the descriptor of an Image - Dockerfile
```Dockerfile
FROM httpd:2.4
COPY ./public-html/ /usr/local/apache2/htdocs/
```

## Dockerfile rules
* Every Dockerfile must have FROM. At least FROM scratch.
* Almost all commands create a new layer. We don't want a lot of them, so chain commands or use a script!
* Each layer is cached: if change something, only the changed statement and things after will be rebuilt.